# Device Frames
<img src="banner.png" width="100%" height="auto" alt="Banner image with the text 'Device Frames' surrounded by various mockups of devices">

Minimalist frames/mockups of various mobile devices. Available in SVG and PNG (with a width of 1930 for mobile and a height of 1930 for tablet).

As seen in promotional material for Gugal and [Disconnect](https://www.youtube.com/watch?v=TFt82qkldgQ), among other apps.

## Frames
Device Frames currently has 3 phone frames and 1 tablet frame.

### Phone
- [**Modern**](phone/modern/) - a modern phone with small, uniform bezels and a centered camera. The screen dimensions roughly match that of the Pixel 6. 
- [**Next**](phone/next/) - a phone with large top and bottom bezels and a rounded display inspired by Android design trends from around 2017-2018.
- [**Retro**](phone/retro/) - a phone with even larger top and bottom bezels and a rectangle screen.

### Tablet
- [**Tablet**](tablet/) - a modern tablet with a large uniform bezel. The screen dimensions roughly match that of the Pixel Tablet.

## License
Device Frames is licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/). This license allows for personal and commercial use, but requires attribution and requires works using Device Frames to be distributed under the same license. To add attribution, add a link to this repository and mention the name, authors and license. See this example:

> This ad uses [Device Frames](https://gitlab.com/cat-aspect/device-frames) by Cat Aspect, licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

A Premium version with less restrictions is available [here](https://cataspect.gumroad.com/l/device-frames).